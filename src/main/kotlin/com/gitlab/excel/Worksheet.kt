package com.gitlab.excel

import com.gitlab.excel.wrapper.Traversable
import org.apache.poi.ss.usermodel.Sheet
import java.lang.Integer.max
import java.lang.Integer.min

class Worksheet(sheet: Sheet): Traversable(sheet.sheetName) {
    override val firstRow: Int
    override val lastRow: Int
    override val firstColumn: Int
    override val lastColumn: Int

    init {
        var firstColumn = 0
        var lastColumn = 0
        firstRow = sheet.firstRowNum
        lastRow = sheet.lastRowNum
        IntRange(firstRow, lastRow).forEach {index ->
            val row = sheet.getRow(index)
            firstColumn = min(firstColumn, row.firstCellNum.toInt())
            lastColumn = max(lastColumn, row.lastCellNum.toInt())
            cells.add((IntRange(0, firstColumn).map { null } + row.map { it.toString().trim() }))
        }

        this.firstColumn = firstColumn
        this.lastColumn = lastColumn
    }
}
