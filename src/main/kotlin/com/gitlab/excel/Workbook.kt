package com.gitlab.excel


import com.gitlab.excel.exceptions.MissingWorksheetException
import com.gitlab.excel.wrapper.Storable
import com.gitlab.excel.wrapper.Traversable
import com.gitlab.excel.wrapper.WorksheetWrapper
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.poi.ss.usermodel.Workbook
import java.io.FileInputStream

interface WorksheetCollection {
    fun worksheet(name: String): Traversable
}

abstract class Workbook(val filename: String, vararg ws: WorksheetWrapper): WorksheetCollection {
    private val workbook: Workbook
    private val worksheets = listOf(*ws)

    init {
        val inputStream = FileInputStream(filename)
        workbook = WorkbookFactory.create(inputStream)
    }

    fun process(): List<Storable> {
        println("Extracting data from Workbook $filename")
        return worksheets.map {
            println("Extracting ${it.sheetName}")
            it.queries()
        }.reduce { acc, list -> acc + list }
    }

    override fun worksheet(name: String): Traversable {
        val sheet = workbook.getSheet(name) ?: throw MissingWorksheetException(name)
        return Worksheet(sheet)
    }
}
