package com.gitlab.excel.wrapper.iter

import com.gitlab.excel.WorksheetCollection
import com.gitlab.excel.wrapper.WorksheetWrapper

abstract class IterableWorksheet(workbook: WorksheetCollection, sheetName: String, traverseByRow: Boolean=true): WorksheetWrapper(workbook, sheetName) {
    internal val indexIterator: IndexIterator = if (traverseByRow)
        IndexIterator(worksheet.firstRow, worksheet.lastRow)
    else
        IndexIterator(worksheet.firstColumn, worksheet.lastColumn)

    fun row(name: String): IterableSingleCellDelegate {
        return IterableSingleCellDelegate(this.worksheet, indexIterator, name, IterableCellDelegate.Direction.Row)
    }

    fun column(name: String): IterableSingleCellDelegate {
        return IterableSingleCellDelegate(this.worksheet, indexIterator, name, IterableCellDelegate.Direction.Column)
    }

    fun groupedColumn(name: String, length: Int): CategoryCellDelegate {
        return CategoryCellDelegate(this.worksheet, indexIterator, name, length, IterableCellDelegate.Direction.Column)
    }

}
