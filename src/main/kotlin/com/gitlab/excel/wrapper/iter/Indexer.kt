package com.gitlab.excel.wrapper.iter

interface Indexer {
    fun next(): Int
    fun hasNext(): Boolean
    fun moveTo(value: Int)
    fun current(): Int
    fun shift(shift: Int): Indexer
    fun stop()
}
