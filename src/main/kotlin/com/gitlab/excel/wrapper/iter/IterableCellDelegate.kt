package com.gitlab.excel.wrapper.iter

import com.gitlab.excel.wrapper.Position
import com.gitlab.excel.wrapper.Traversable

abstract class IterableCellDelegate(val worksheet: Traversable, protected val indexIterator: Indexer, val name: String, val index: Position, val direction: Direction) {
    enum class Direction {
        Row,
        Column
    }

    protected fun getCell(offset: Position = Position(0, 0)): String? {
        if (direction == Direction.Row) {
            val column = indexIterator.current() + offset.column
            val row = index.row + offset.row
            return worksheet[row, column]
        }
        val column = index.column + offset.column
        val row = indexIterator.current() + offset.row
        return worksheet[row, column]
    }
}
