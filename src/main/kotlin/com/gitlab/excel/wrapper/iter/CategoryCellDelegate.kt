package com.gitlab.excel.wrapper.iter

import com.gitlab.excel.exceptions.CellNotFoundException
import com.gitlab.excel.exceptions.EmptyCellException
import com.gitlab.excel.wrapper.Position
import com.gitlab.excel.wrapper.Traversable
import kotlin.reflect.KProperty

class CategoryCellDelegate(worksheet: Traversable, indexIterator: Indexer, name: String, private val subCategoryCount: Int, index: Position, direction: Direction):
IterableCellDelegate(worksheet, indexIterator.shift(1), name, index, direction) {
    val subCategoryNames: List<String> = if (direction == Direction.Column) {
        IntRange(0, subCategoryCount - 1).map {
            worksheet[index.row + 1, index.column + it] ?: ""
        }
    } else {
        IntRange(0, subCategoryCount - 1).map {
            worksheet[index.row + it, index.column + 1] ?: ""
        }
    }

    constructor(worksheet: Traversable, indexIterator: Indexer, name: String, count: Int, direction: Direction): this(worksheet, indexIterator, name, count, worksheet.find(name), direction)


    fun onRowAbove(other: String): CategoryCellDelegate {
        val origin = worksheet.find(other)
        val position = worksheet.findInRow(name, origin.row - 1)

        if (position != null) {
            return CategoryCellDelegate(worksheet, indexIterator, name, subCategoryCount, position, direction)
        }
        throw CellNotFoundException(name)
    }

    fun double(): Delegate<Double> {
        return Delegate {
            if (it == null) {
                throw EmptyCellException(name, index)
            }
            it.toDoubleOrNull() ?: 0.0
        }
    }

    open inner class Delegate<T: Any?> {
        private val converter: (T) -> T
        private val cast: (String?) -> T


        operator fun getValue(thisRef: Any?, property: KProperty<*>): Map<String, T> {
            return IntRange(0, subCategoryCount - 1).map {
                val offset = if (direction == Direction.Column) {
                    Position(0, it)
                } else {
                    Position( it, 0)
                }
                subCategoryNames[it] to converter(cast(getCell(offset)))
            }.toMap()
        }

        fun transformed(transform: (T) -> T): Delegate<T> {
            return Delegate(cast, transform)
        }

        constructor(cast: (String?) -> T): this(cast,  { v -> v })

        private constructor(cast: (String?) -> T, transform: (T) -> T) {
            this.cast = cast
            this.converter = transform
        }
    }
}
