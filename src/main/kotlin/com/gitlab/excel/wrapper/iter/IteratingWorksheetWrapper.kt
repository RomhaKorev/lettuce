package com.gitlab.excel.wrapper.iter

import com.gitlab.excel.WorksheetCollection
import com.gitlab.excel.exceptions.StopIterationException
import com.gitlab.excel.wrapper.Storable


abstract class IteratingWorksheetWrapper<T: Storable>(workbook: WorksheetCollection, sheetName: String, traverseByRow: Boolean=true): IterableWorksheet(workbook, sheetName, traverseByRow) {

    protected val produced: MutableList<T> = mutableListOf()

    override fun queries(): List<Storable> {
        val queries = produceAll()
        return preBatchQueries() + queries + postBatchQueries()
    }

    private fun produceAll(): List<T> {
        indexIterator.forEach {
        _ ->
            handleExceptions {
                produce()?.let { storable -> produced.add(storable) }
            }
            handleExceptions {
                produceList().let { listStorable -> produced.addAll(listStorable) }
            }
        }
        return produced
    }

    abstract fun produce(): T?

    protected open fun produceList(): List<T> {
        return emptyList()
    }


    private fun handleExceptions(f: () -> Unit) {
        return try {
            f()
        } catch (e: StopIterationException) {
            println("Processing stopped due to the error: " + e.message)
            indexIterator.stop()
        }
    }
}
