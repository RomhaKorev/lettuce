package com.gitlab.excel.wrapper.iter

import java.lang.Integer.max
import java.lang.Integer.min

class IndexIterator(private val first: Int, private val last: Int): Iterator<Int>, Indexer {
    override fun stop() {
        current = last
    }

    override fun next(): Int {
        if (!hasNext())
            return current
        return current++
    }

    override fun hasNext(): Boolean {
        return current < last
    }

    private var current: Int = first

    override fun moveTo(value: Int) {
        current = min(last, max(first, value))
    }

    override fun current(): Int {
        return current
    }

    override fun shift(shift: Int): Indexer {
        moveTo(current + shift)
        return this
    }
}

