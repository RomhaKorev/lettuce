package com.gitlab.excel.wrapper.iter

import com.gitlab.excel.exceptions.CellNotFoundException
import com.gitlab.excel.exceptions.EmptyCellException
import com.gitlab.excel.exceptions.EmptyStringException
import com.gitlab.excel.exceptions.StopIterationException
import com.gitlab.excel.wrapper.Position
import com.gitlab.excel.wrapper.Traversable
import kotlin.reflect.KProperty

open class IterableSingleCellDelegate(worksheet: Traversable, indexIterator: Indexer, name: String, index: Position, direction: Direction):
IterableCellDelegate(worksheet, indexIterator, name, index, direction){

    constructor(worksheet: Traversable, indexIterator: Indexer, name: String, direction: Direction): this(worksheet, indexIterator, name, worksheet.find(name), direction)

    fun onSameRowThan(other: String): IterableSingleCellDelegate {
        val origin = worksheet.find(other)
        val position = worksheet.findInRow(name, origin.row)

        if (position != null) {
            return IterableSingleCellDelegate(worksheet, this.indexIterator, name, position, direction)
        }
        throw CellNotFoundException(name)
    }

    fun double(): Delegate<Double> {
        return Delegate {
            if (it == null) {
                throw EmptyCellException(name, index)
            }
            it.toDoubleOrNull() ?: 0.0
        }
    }

    fun nullableDouble(): Delegate<Double?> {
        return Delegate {
            it?.toDoubleOrNull()
        }
    }

    fun long(): Delegate<Long> {
        return Delegate {
            if (it == null) {
                throw EmptyCellException(name, index)
            }
            it.toLongOrNull() ?: 0L
        }
    }

    fun string(): StringDelegate {
        return StringDelegate()
    }


    open inner class Delegate<T: Any?> {
        private val converter: (T) -> T
        private val cast: (String?) -> T


        open operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
            return converter(cast(getCell()))
        }

        fun transformed(transform: (T) -> T): Delegate<T> {
            return Delegate(cast, transform)
        }

        fun stopIterationOnFailed(): Delegate<T> {
            return Delegate({
                try {
                    cast(it)
                } catch (e: EmptyCellException) {
                    throw StopIterationException(e.message?: "", e)
                }
            }, converter)
        }

        constructor(cast: (String?) -> T): this(cast,  { v -> v })

        private constructor(cast: (String?) -> T, transform: (T) -> T) {
            this.cast = cast
            this.converter = transform
        }
    }


    inner class StringDelegate(private val transformation: (String?) -> String? = { it -> it},
                               private val notEmpty: Boolean = false):
            Delegate<String>({
                if (it == null) {
                    throw EmptyCellException(name, index)
                }
                if (it == "" && notEmpty) {
                    throw EmptyStringException(name, index)
                }

                transformation(it)?: ""
            })
    {
        fun notEmpty(): StringDelegate {
            return StringDelegate(transformation, true)
        }
    }
}
