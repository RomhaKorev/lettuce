package com.gitlab.excel.wrapper

data class Position(val row: Int, val column: Int) {
    operator fun plus(other: Position): Position {
        return Position(row + other.row, column + other.column)
    }

    override fun toString(): String {
        return "($row; $column)"
    }
}
