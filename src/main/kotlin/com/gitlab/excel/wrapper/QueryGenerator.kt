package com.gitlab.excel.wrapper


interface QueryGenerator {
    fun queries(): List<Storable>
    fun preBatchQueries(): List<Storable>
    fun postBatchQueries(): List<Storable>
}
