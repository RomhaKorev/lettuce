package com.gitlab.excel.wrapper

import com.gitlab.excel.exceptions.EmptyCellException
import kotlin.reflect.KProperty


open class CellDelegate(protected val worksheet: Traversable, protected val index: Position, protected val name: String) {
    private constructor(worksheet: Traversable, name: String, offsetRow: Int, offsetColumn: Int):
            this(worksheet, worksheet.find(name) + Position(offsetRow, offsetColumn), name)

    constructor(worksheet: Traversable, name: String):
            this(worksheet, worksheet.find(name), name)

    fun translated(row: Int, column: Int): CellDelegate {
        return CellDelegate(worksheet, name, row, column)
    }

    fun double(): NumberCellDelegate<Double> {
        return NumberCellDelegate(worksheet, index, name) { str, multiplier -> if (str == "") 0.0 else str.toDouble() * multiplier }
    }

    fun string(): StringCellDelegate {
        return StringCellDelegate(worksheet, index, name)
    }

    protected fun getCell(): String? {
        println("Accessing to the cell '$name' at $index")
        return worksheet[index.row, index.column]
    }
}


class StringCellDelegate internal constructor(worksheet: Traversable, index: Position, name: String) :
    CellDelegate(worksheet, index, name) {

    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return getCell() ?: throw EmptyCellException(name, index)
    }
}

class NumberCellDelegate<T: Number>: CellDelegate {
    private var multiplier: Double = 1.0
    val converter: (String, Double) -> T

    internal constructor(worksheet: Traversable, index: Position, name: String, converter: (String, Double) -> T) : super(worksheet, index, name) {
        this.converter = converter
    }

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        val cell = getCell() ?: throw EmptyCellException(name, index)
        return converter(cell, multiplier)
    }

    fun withMultiplier(multiplier: T): NumberCellDelegate<T> {
        return NumberCellDelegate(worksheet, index, name, multiplier.toDouble(), converter)
    }

    private constructor(worksheet: Traversable, index: Position, name: String, multiplier: Double, converter: (String, Double) -> T): this(worksheet, index, name, converter) {
        this.multiplier = multiplier
    }
}
