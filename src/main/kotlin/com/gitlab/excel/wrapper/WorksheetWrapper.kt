package com.gitlab.excel.wrapper

import com.gitlab.excel.WorksheetCollection


abstract class WorksheetWrapper(workbook: WorksheetCollection, val sheetName: String): WorksheetAccessWrapper(workbook.worksheet(sheetName)), QueryGenerator {
    fun below(name: String): CellDelegate {
        return CellDelegate(this.worksheet, name).translated(1, 0)
    }

    fun toTheRight(name: String, offset: Int = 1): CellDelegate {
        return CellDelegate(this.worksheet, name).translated(0, offset)
    }
}
