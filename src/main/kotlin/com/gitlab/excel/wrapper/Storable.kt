package com.gitlab.excel.wrapper

abstract class Storable(val typeName: String) {
    abstract fun toQuery(): String
}
