package com.gitlab.excel.wrapper

import com.gitlab.excel.exceptions.CellNotFoundException

abstract class Traversable(private val name: String) {

    protected val cells: MutableList<List<String?>> = mutableListOf()

    operator fun get(r: Int, c: Int): String? {
        return cells.getOrElse(r) { emptyList() }.getOrNull(c)
    }

    fun findInRow(content: String, rowIndex: Int): Position? {
        (firstColumn .. lastColumn).forEach { columnIndex ->
            val cell = this[rowIndex, columnIndex]
            if (cell == content)
                return Position(rowIndex, columnIndex)
        }
        return null
    }

    fun find(content: String): Position {
        try {
            return (firstRow..lastRow).mapNotNull { rowIndex ->
                findInRow(content, rowIndex)
            }.first()
        } catch(ex: NoSuchElementException) {
            println("The cell named '$content' cannot be found")
            throw CellNotFoundException(content)
        }
    }


    abstract val firstRow: Int
    abstract val lastRow: Int
    abstract val firstColumn: Int
    abstract val lastColumn: Int
}
