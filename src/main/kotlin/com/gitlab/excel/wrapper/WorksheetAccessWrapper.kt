package com.gitlab.excel.wrapper

open class WorksheetAccessWrapper(val worksheet: Traversable) {
    operator fun get(row: Int, column: Int): String? {
        return worksheet[row, column]
    }

    fun find(content: String): Position {
        return worksheet.find(content)
    }

    internal fun findInRow(content: String, rowIndex: Int): Position? {
        return worksheet.findInRow(content, rowIndex)
    }
}
