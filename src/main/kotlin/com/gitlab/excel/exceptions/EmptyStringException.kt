package com.gitlab.excel.exceptions

import com.gitlab.excel.wrapper.Position

class EmptyStringException(name: String, index: Position): RuntimeException("The item '$name' should not be empty at $index")
