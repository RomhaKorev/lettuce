package com.gitlab.excel.exceptions

class StopIterationException(reason: String, cause: Throwable): RuntimeException(reason, cause)
