package com.gitlab.excel.exceptions

import com.gitlab.excel.wrapper.Position

class EmptyCellException(name: String, index: Position): RuntimeException("The cell $name is empty at position $index") {
}
