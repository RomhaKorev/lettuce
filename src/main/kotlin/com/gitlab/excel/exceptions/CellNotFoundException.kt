package com.gitlab.excel.exceptions

class CellNotFoundException(content: String): RuntimeException("Cannot find a cell containing '$content'")
