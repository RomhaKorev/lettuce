package com.gitlab.excel.exceptions

class MissingWorksheetException(name: String): Exception("The worksheet '$name' does not exist")
