package com.gitlab.excel

import com.gitlab.excel.wrapper.CellDelegate
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class CellDelegateTests {

    lateinit  var currentWs: SheetStub

    @Test
    fun `Can retrieve a double below a cell`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 2 3
        """) column "B").translated(1, 0)
        val value by delegate.double()
        assertEquals(2.0, value)
    }

    @Test
    fun `Can retrieve a string to the right of a cell`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 2 3
        """) column "B").translated(0, 1)
        val value by delegate.string()
        assertEquals("C", value)
    }

    @Test
    fun `Can apply a multiplier to a double`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 2 3
        """) column "B").translated(1, 0)
        val value by delegate.double().withMultiplier(100.0)
        assertEquals(200.0, value)
    }

    inner class Helper(content: String) {
        init {
            currentWs = SheetStub(content.trimIndent())
        }

        infix fun column(name: String): CellDelegate {
            return CellDelegate(currentWs, name)
        }

        infix fun row(name: String): CellDelegate {
            return CellDelegate(currentWs, name)
        }
    }

    private fun onTheSheet(content: String): Helper {
        return Helper(content)
    }
}
