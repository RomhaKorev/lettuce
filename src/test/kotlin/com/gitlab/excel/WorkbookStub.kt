package com.gitlab.excel

import com.gitlab.excel.wrapper.Traversable

class WorkbookStub(s: Map<String, String>): WorksheetCollection {
    private val sheets: Map<String, Traversable>

    init {
        sheets = s.mapValues { i -> SheetStub(i.value.trimIndent()) }
    }
    override fun worksheet(name: String): Traversable {
        return sheets[name] ?: error("$name not found")
    }

}
