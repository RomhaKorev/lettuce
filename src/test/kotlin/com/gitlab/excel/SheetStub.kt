package com.gitlab.excel

import com.gitlab.excel.wrapper.Traversable

class SheetStub(content: String) : Traversable("STUB") {
    override val firstRow: Int = 0
    override val lastRow: Int
    override val firstColumn: Int = 0
    override val lastColumn: Int

    init {
        var last = 0
        lastRow = content.lines().count() - 1
        content.lines().forEach {
            val row = it.split(' ')
            last = kotlin.math.max(row.count() - 1, last)
            cells.add(row)
        }
        lastColumn = last
    }


}
