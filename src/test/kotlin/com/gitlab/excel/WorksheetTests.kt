package com.gitlab.excel

import com.gitlab.excel.exceptions.CellNotFoundException
import com.gitlab.excel.wrapper.Position
import com.gitlab.excel.wrapper.Storable
import com.gitlab.excel.wrapper.WorksheetAccessWrapper
import com.gitlab.excel.wrapper.WorksheetWrapper
import com.gitlab.excel.wrapper.iter.IteratingWorksheetWrapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows


class WorksheetTests {
    val wb = WorkbookStub(mapOf(
            "Sheet 1" to """
            A B C
            1 2 3
            4 5 6
            7 8 9
            """,
            "Sheet 2" to """
            D z y x
            E 6 7 8
            F 1 2 3
            """,
            "Sheet 3" to """
            A ? C ?
            1 ? 3 ?
            B 2 ? ?
            """,
            "Sheet 4" to """
            A B C ?
            ? ? Z B
            ? ? ? 11
            ? ? ? 12
            ? ? ? 13
            """,

            "Sheet 5" to """
            A B . . C
            ? x y z ?
            ? 1 2 3 ?
            ? 4 5 6 ?
            """,

            "Sheet 6" to """
            A B . . C
            ? x y z ?
            ? 1 2 3 ?
            ? 4 5 6 ?
            """
    ))

    inner class Sheet1: IteratingWorksheetWrapper<Sheet1.Data>(wb,"Sheet 1") {
        inner class Data(val a: Long, val b: Long, val c: Long): Storable("fake") {
            override fun toQuery(): String {
                return "$a $b $c"
            }
        }

        override fun produce(): Data? {
            return Data(a, b, c)
        }

        override fun preBatchQueries(): List<Storable> {
            return emptyList()
        }

        override fun postBatchQueries(): List<Storable> {
            return emptyList()
        }

        val a by column("A").long()
        val b by column("B").long()
        val c by column("C").long()
    }

    inner class Sheet2: IteratingWorksheetWrapper<Sheet2.Data>(wb,"Sheet 2", false) {
        inner class Data(val a: String, val b: Long, val c: Double): Storable("fake") {
            override fun toQuery(): String {
                return "$a $b $c"
            }
        }
        override fun produce(): Data? {
            return Data(d, e, f)
        }

        override fun preBatchQueries(): List<Storable> {
            return emptyList()
        }

        override fun postBatchQueries(): List<Storable> {
            return emptyList()
        }

        val d by row("D").string()
        val e by row("E").long()
        val f by row("F").double()
    }

    inner class Sheet3: WorksheetWrapper(wb,"Sheet 3") {
        override fun queries(): List<Storable> {
            return listOf(Data(a, b, c))
        }

        inner class Data(val a: Double, val b: String, val c: String): Storable("fake") {
            override fun toQuery(): String {
                return "$a $b $c"
            }
        }

        override fun preBatchQueries(): List<Storable> {
            return emptyList()
        }

        override fun postBatchQueries(): List<Storable> {
            return emptyList()
        }

        val a by below("A").double()
        val b by toTheRight("B").string()
        val c by below("C").string()
    }

    inner class SheetFails: WorksheetWrapper(wb,"Sheet 1") {
        override fun queries(): List<Storable> {
            return listOf()
        }

        override fun preBatchQueries(): List<Storable> {
            return emptyList()
        }

        override fun postBatchQueries(): List<Storable> {
            return emptyList()
        }
        val fail by below("ZZZZ").string()
    }


    inner class Sheet4: IteratingWorksheetWrapper<Sheet4.Data>(wb,"Sheet 4") {
        inner class Data(val b: Long): Storable("fake") {
            override fun toQuery(): String {
                return "$b"
            }
        }

        override fun produce(): Data? {
            return Data(b)
        }

        override fun preBatchQueries(): List<Storable> {
            return emptyList()
        }

        override fun postBatchQueries(): List<Storable> {
            return emptyList()
        }

        val b by column("B").onSameRowThan("Z").long()
    }

    inner class Sheet5: IteratingWorksheetWrapper<Sheet5.Data>(wb,"Sheet 5") {
        inner class Data(val group: Map<String, Double>): Storable("fake") {
            override fun toQuery(): String {
                return "${group["x"]} ${group["y"]} ${group["z"]}"
            }
        }

        override fun produce(): Data? {
            return Data(group)
        }

        override fun preBatchQueries(): List<Storable> {
            return emptyList()
        }

        override fun postBatchQueries(): List<Storable> {
            return emptyList()
        }

        val group by groupedColumn("B", 3).double()
    }

    inner class Sheet6: IteratingWorksheetWrapper<Sheet6.Data>(wb,"Sheet 5") {
        inner class Data(val group: Map<String, Double>): Storable("fake") {
            override fun toQuery(): String {
                return "${group["x"]} ${group["y"]} ${group["z"]}"
            }
        }

        override fun produce(): Data? {
            return Data(group)
        }

        override fun preBatchQueries(): List<Storable> {
            return emptyList()
        }

        override fun postBatchQueries(): List<Storable> {
            return emptyList()
        }

        val group: Map<String, Double> by groupedColumn("B", 3).double().transformed { it * 100.0 }
    }

    @Test
    fun `Iterating Worksheet can produce data by column`() {
        val ws = Sheet1()
        assertEquals(listOf("1 2 3", "4 5 6", "7 8 9"), ws.queries().map { it.toQuery() })
    }

    @Test
    fun `Iterating Worksheet can produce data by row`() {
        val ws = Sheet2()
        assertEquals(listOf("z 6 1.0", "y 7 2.0", "x 8 3.0"), ws.queries().map { it.toQuery() })
    }

    @Test
    fun `Worksheet can produce data by cell`() {
        val ws = Sheet3()
        assertEquals(listOf("1.0 2 3"), ws.queries().map { it.toQuery() })
    }

    @Test
    fun `Can find directly to the cells`() {
        val ws = WorksheetAccessWrapper(wb.worksheet("Sheet 1"))
        assertEquals(Position(0, 1), ws.find("B"))
    }

    @Test
    fun `Can access directly to the cells`() {
        val ws = WorksheetAccessWrapper(wb.worksheet("Sheet 1"))
        assertEquals("2", ws[1, 1])
    }

    @Test
    fun `Will throw an error if a cell is not found`() {
        assertThrows<CellNotFoundException> { SheetFails() }
    }


    @Test
    fun `Will take the column regarding the reference`() {
        val ws = Sheet4()
        assertEquals(listOf("0", "11", "12", "13"), ws.queries().map { it.toQuery() })
    }

    @Test
    fun `Can produce by grouped column`() {
        val ws = Sheet5()
        assertEquals(listOf("1.0 2.0 3.0", "4.0 5.0 6.0"), ws.queries().map { it.toQuery() })
    }

    @Test
    fun `Can produce by grouped column with multiplier`() {
        val ws = Sheet6()
        assertEquals(listOf("100.0 200.0 300.0", "400.0 500.0 600.0"), ws.queries().map { it.toQuery() })
    }
}
