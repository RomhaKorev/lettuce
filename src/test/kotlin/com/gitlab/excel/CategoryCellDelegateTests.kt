package com.gitlab.excel

import com.gitlab.excel.wrapper.iter.CategoryCellDelegate
import com.gitlab.excel.wrapper.iter.IndexIterator
import com.gitlab.excel.wrapper.iter.IterableCellDelegate
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class CategoryCellDelegateTests {

    lateinit  var currentWs: SheetStub
    lateinit var currentIterator: IndexIterator

    @Test
    fun `Can retrieve sub category name by column`() {
        val delegate = givenContent(
                """
            A B     C
            ? z y x ?
            ? 1 2 3 ?
            ? 4 5 6 ?
        """).column("B", 3)
        assertEquals(listOf("z", "y", "x"), delegate.subCategoryNames)
    }

    @Test
    fun `Can retrieve sub category name by row`() {
        val delegate = givenContent(
                """
            A ? ? ?
            B z 1 4
            . y 2 5
            . x 3 6
            C ? ? ?
        """).row("B", 3)
        assertEquals(listOf("z", "y", "x"), delegate.subCategoryNames)
    }

    @Test
    fun `Can retrieve data by column`() {
        val delegate = givenContent(
                """
            A B . . C
            ? z y x ?
            ? 1 2 3 ?
            ? 4 5 6 ?
        """).column("B", 3)

        val value by delegate.double()

        val values = mutableListOf<Map<String, Double>>()
        currentIterator.forEach {
            _ -> values.add(value)
        }
        assertEquals(listOf(
                mapOf(
                        "z" to 1.0,
                        "y" to 2.0,
                        "x" to 3.0
                ),
                mapOf(
                        "z" to 4.0,
                        "y" to 5.0,
                        "x" to 6.0
                )
        ), values)
    }

    inner class Helper(content: String) {
        init {
            currentWs = SheetStub(content.trimIndent())
        }

        fun column(name: String, count: Int): CategoryCellDelegate {
            currentIterator = IndexIterator(currentWs.firstRow, currentWs.lastRow)
            return CategoryCellDelegate(currentWs, currentIterator, name, count, IterableCellDelegate.Direction.Column)
        }

        fun row(name: String, count: Int): CategoryCellDelegate {
            currentIterator = IndexIterator(currentWs.firstColumn, currentWs.lastColumn)
            return CategoryCellDelegate(currentWs, currentIterator, name, count, IterableCellDelegate.Direction.Row)
        }
    }

    private fun givenContent(content: String): Helper {
        return Helper(content)
    }
}
