package com.gitlab.excel

import com.gitlab.excel.wrapper.iter.IndexIterator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test


class IteratorTest {


    @Test
    fun `Can iterate`() {
        val it = IndexIterator(0, 5)
        assertEquals(0, it.current())
        it.next()
        assertEquals(1, it.current())
    }

    @Test
    fun `can check if I can iterate again`() {
        val it = IndexIterator(0, 5)
        assertEquals(0, it.current())
        assertTrue(it.hasNext())
        it.next()
        assertEquals(1, it.current())
    }

    @Test
    fun `Cannot iterate outside the bound`() {
        val it = IndexIterator(0, 1)
        it.next()
        assertEquals(1, it.current())
        it.next()
        assertEquals(1, it.current())
    }

    @Test
    fun `Can translate the iterator`() {
        val it = IndexIterator(0, 5)
        it.moveTo(3)
        assertEquals(3, it.current())
    }

    @Test
    fun `Cannot translate the iterator outside the bounds`() {
        val it = IndexIterator(0, 5)
        it.moveTo(8)
        assertEquals(5, it.current())
    }
}
