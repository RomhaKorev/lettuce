package com.gitlab.excel

import com.gitlab.excel.wrapper.Storable
import com.gitlab.excel.wrapper.iter.IteratingWorksheetWrapper


class Base(val a: Long, val b: Long, val c: String): Storable("Test") {
    override fun toQuery(): String {
        return "$a, $b, $c"
    }
}

class IteratingWorksheetBasic(stub: WorkbookStub) : IteratingWorksheetWrapper<Base>(stub, "base"){

    private val a by column("A").long()
    private val b by column("B").onSameRowThan("A").long()
    private val c by column("C").onSameRowThan("A").string()
    override fun produce(): Base? {
        return Base(a, b, c)
    }

    override fun preBatchQueries(): List<Storable> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun postBatchQueries(): List<Storable> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
