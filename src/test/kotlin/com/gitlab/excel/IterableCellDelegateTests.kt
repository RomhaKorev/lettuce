package com.gitlab.excel

import com.gitlab.excel.exceptions.CellNotFoundException
import com.gitlab.excel.exceptions.EmptyCellException
import com.gitlab.excel.exceptions.EmptyStringException
import com.gitlab.excel.wrapper.Position
import com.gitlab.excel.wrapper.iter.IndexIterator
import com.gitlab.excel.wrapper.iter.IterableCellDelegate
import com.gitlab.excel.wrapper.iter.IterableSingleCellDelegate
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows


fun doNothing(@Suppress("UNUSED_PARAMETER") param: Any?) {

}


class IterableCellDelegateTests {

    lateinit  var currentWs: SheetStub
    lateinit var currentIterator: IndexIterator
    @Test
    fun `Can find column by name`() {
        val delegate = onTheSheet(
                """
            A B C
            1 2 3
        """) column "B"
        assertEquals(Position(0, 1), delegate.index)
    }


    @Test
    fun `Can find row by name`() {
        val delegate = onTheSheet(
                """
            A 1 2 3
            B 4 5 6
            C 7 8 9
        """) row "B"
        assertEquals(Position(1, 0), delegate.index)
    }

    @Test
    fun `Can find column by name relative to another one`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 2 3
            R Z Z B
            4 5 6 7
        """) column "B").onSameRowThan("R")
        assertEquals(Position(2, 3), delegate.index)
    }

    @Test
    fun `Raise exception if not found`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 2 3
            R Z Z B
            4 5 6 7
        """) column "C")
        Assertions.assertThrows(CellNotFoundException::class.java) {delegate.onSameRowThan("R")}
    }

    @Test
    fun `Can iterate on a double`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 4 a
            2 5 b
            3 6 c
        """) column "A")
        val value: Double by delegate.double()

        val values = mutableListOf<Double>()
        currentIterator.forEach {
            _ -> values.add(value)
        }

        assertEquals(listOf(1.0, 2.0, 3.0), values)
    }

    @Test
    fun `Can iterate on a long`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 4 a
            2 5 b
            3 6 c
        """) column "B")
        val value: Long by delegate.long()

        val values = mutableListOf<Long>()
        currentIterator.forEach {
            _ -> values.add(value)
        }

        assertEquals(listOf(4L, 5L, 6L), values)
    }

    @Test
    fun `Can iterate on a string`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 4 a
            2 5 b
            3 6 cdef
        """) column "C")
        val value: String by delegate.string()

        val values = mutableListOf<String>()
        currentIterator.forEach {
            _ -> values.add(value)
        }

        assertEquals(listOf("a", "b", "cdef"), values)
    }

    @Test
    fun `Can iterate by Row`() {
        val delegate = (onTheSheet(
                """
            A 1 2 3
            B a b cdef
        """) row "B")
        val value: String by delegate.string()

        val values = mutableListOf<String>()
        currentIterator.forEach {
            _ -> values.add(value)
        }

        assertEquals(listOf("a", "b", "cdef"), values)
    }

    @Test
    fun `Can iterate on a nullable double`() {
        val delegate = (onTheSheet(
                """
            A B C D
            1 4 a 7
            2 5 b 
            3 6 c 9
        """) column "D")
        val value: Double? by delegate.nullableDouble()

        val values = mutableListOf<Double?>()
        currentIterator.forEach {
            _ -> values.add(value)
        }

        assertEquals(listOf(7.0, null, 9.0), values)
    }

    @Test
    fun `Fails on an empty string`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 4 a
            2 5 
            3 6 cdef
        """) column "C")
        val value: String by delegate.string().notEmpty()

        Assertions.assertThrows(EmptyStringException::class.java) {
            currentIterator.forEach {
            _ ->
                doNothing(value)
            }
        }
    }

    @Test
    fun `Can transform a string`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 4 a
            2 5 b
            3 6 cdef
        """) column "C")
        val value: String by delegate.string().transformed { it.toUpperCase() }

        val values = mutableListOf<String>()
        currentIterator.forEach {
            _ -> values.add(value)
        }

        assertEquals(listOf("A", "B", "CDEF"), values)
    }


    @Test
    fun `Fails on empty cell for double`() {
        val delegate = (onTheSheet(
                """
            A B C D
            1 4 a 7
            2 5 b
            3 6 c 9
        """) column "D")
        val value: Double by delegate.double()

        Assertions.assertThrows(EmptyCellException::class.java) {
            currentIterator.forEach {
                _ ->
                doNothing(value)
            }
        }
    }

    @Test
    fun `Fails on empty cell for Long`() {
        val delegate = (onTheSheet(
                """
            A B C D
            1 4 a 7
            2 5 b
            3 6 c 9
        """) column "D")
        val value: Long by delegate.long()

        Assertions.assertThrows(EmptyCellException::class.java) {
            currentIterator.forEach {
                _ ->
                doNothing(value)
            }
        }
    }



    @Test
    fun `Fails on empty cell for string`() {
        val delegate = (onTheSheet(
                """
            A B C D
            1 4 a 7
            2 5 b
            3 6 c 9
        """) column "D")
        val value: String by delegate.string()

        Assertions.assertThrows(EmptyCellException::class.java) {
            currentIterator.forEach {
                _ ->
                doNothing(value)
            }
        }
    }


    @Test
    fun `Can iterate on a double with multiplier`() {
        val delegate = (onTheSheet(
                """
            A B C
            1 4 a
            2 5 b
            3 6 c
        """) column "A")
        val value: Double by delegate.double().transformed {it * 100.0}

        val values = mutableListOf<Double>()
        currentIterator.forEach {
            _ -> values.add(value)
        }

        assertEquals(listOf(100.0, 200.0, 300.0), values)
    }

    inner class Helper(content: String) {
        init {
            currentWs = SheetStub(content.trimIndent())
        }

        infix fun column(name: String): IterableSingleCellDelegate {
            currentIterator = IndexIterator(currentWs.firstRow, currentWs.lastRow)
            return IterableSingleCellDelegate(currentWs, currentIterator, name, IterableCellDelegate.Direction.Column)
        }

        infix fun row(name: String): IterableSingleCellDelegate {
            currentIterator = IndexIterator(currentWs.firstColumn, currentWs.lastColumn)
            return IterableSingleCellDelegate(currentWs, currentIterator, name, IterableCellDelegate.Direction.Row)
        }
    }

    private fun onTheSheet(content: String): Helper {
        return Helper(content)
    }
}
